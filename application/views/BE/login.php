<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ABZ ADMIN | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/AdminLTE.min.css"); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/iCheck/square/blue.css"); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
	html {
		position: relative;
		min-height: 100%;
	}
	.carousel-fade .carousel-inner .item {
		opacity: 0;
		transition-property: opacity;
	}
	.carousel-fade .carousel-inner .active {
		opacity: 1;
	}
	.carousel-fade .carousel-inner .active.left,
	.carousel-fade .carousel-inner .active.right {
		left: 0;
		opacity: 0;
		z-index: 1;
	}
	.carousel-fade .carousel-inner .next.left,
	.carousel-fade .carousel-inner .prev.right {
		opacity: 1;
	}
	.carousel-fade .carousel-control {
		z-index: 2;
	}
	@media all and (transform-3d),
	(-webkit-transform-3d) {
		.carousel-fade .carousel-inner > .item.next,
		.carousel-fade .carousel-inner > .item.active.right {
			opacity: 0;
			-webkit-transform: translate3d(0, 0, 0);
			transform: translate3d(0, 0, 0);
		}
		.carousel-fade .carousel-inner > .item.prev,
		.carousel-fade .carousel-inner > .item.active.left {
			opacity: 0;
			-webkit-transform: translate3d(0, 0, 0);
			transform: translate3d(0, 0, 0);
		}
		.carousel-fade .carousel-inner > .item.next.left,
		.carousel-fade .carousel-inner > .item.prev.right,
		.carousel-fade .carousel-inner > .item.active {
			opacity: 1;
			-webkit-transform: translate3d(0, 0, 0);
			transform: translate3d(0, 0, 0);
		}
	}
	.item:nth-child(1) {
        //background: url(https://ununsplash.imgix.net/reserve/oY3ayprWQlewtG7N4OXl_DSC_5225-2.jpg?q=75&fm=jpg&s=85ab821f3fa535036a68155aab571bad) no-repeat center center fixed;
        background: url(/viajes/assets/imgs/pexels-photo-160201.jpeg) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.item:nth-child(2) {
        background: url(/viajes/assets/imgs/pexels-photo-248797.jpeg) no-repeat center center fixed;
        //background: url(https://unsplash.imgix.net/photo-1414073875831-b47709631146?q=75&fm=jpg&s=731b6d5150eea8bafa63ae8194e72ebd) no-repeat center center fixed;
        -webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.item:nth-child(3) {
        background: url(https://ununsplash.imgix.net/photo-1417021423914-070979c8eb34?q=75&fm=jpg&s=55746bd56e02a131b1e48c12196ea866) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.carousel {
		z-index: -99;
	}
	.carousel .item {
		position: fixed;
		width: 100%;
		height: 100%;
	}
	.title {
	  text-align: center;
	  margin-top: 20px;
	  padding: 10px;
	  text-shadow: 2px 2px #000;
	  color: #FFF;
	}
</style>
</head>
<body class="hold-transition login-page">


<div class="carousel slide carousel-fade" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
        </div>
        <div class="item">
        </div>
        <div class="item">
        </div>
    </div>
</div>

<div class="login-box">
    <div class="login-logo">
        <a href="<?php base_url(); ?>"><b>ABZ</b> ADMIN</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Ingrese sus datos para iniciar sesión</p>

        <?php echo validation_errors(); ?>

        <?php echo form_open(base_url('login/nueva_sesion')); ?>
        <div class="form-group has-feedback">
            <input type="email" name="aname" class="form-control" placeholder="Email" required="required">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="apass" class="form-control" placeholder="Contraseña" autocomplete="off" required="required">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <!--                <div class="col-xs-8">-->
            <!--                    <div class="checkbox icheck">-->
            <!--                        <label>-->
            <!--                            <input type="checkbox"> Remember Me-->
            <!--                        </label>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!-- /.col -->
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">ACCESAR</button>
            </div>
            <!-- /.col -->
        </div>
        <?php echo form_close(); ?>

        <?php
            if ($this->session->flashdata('usuario_incorrecto')) {
                echo $this->session->flashdata('usuario_incorrecto');
            }
        ?>


        <!--        <div class="social-auth-links text-center">-->
        <!--            <p>- OR -</p>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using-->
        <!--                Facebook</a>-->
        <!--            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using-->
        <!--                Google+</a>-->
        <!--        </div>-->
        <!-- /.social-auth-links -->

        <!--        <a href="#">I forgot my password</a><br>-->
        <!--        <a href="register.html" class="text-center">Register a new membership</a>-->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url("assets/plugins/iCheck/icheck.min.js"); ?>"></script>
<script>
	$('.carousel').carousel();
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
