<!DOCTYPE html>
<html lang="es">
<head>
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>

    <?php foreach ($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>"/>
    <?php endforeach; ?>
    <?php foreach ($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    <!-- datepicker -->
    <script src="<?php echo base_url("assets/plugins/jQuery_mio/jquery-ui.js"); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/jQuery_mio/jquery-ui.css"); ?>">
    <script src="<?php echo base_url("assets/plugins/jQuery_mio/jquery.canvasjs.min.js"); ?>"></script>

    <!-- calendar -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.print.min.css"); ?>"
          media='print'>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/lib/moment.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/locale-all.js"); ?>"></script>


    <script src="<?php echo base_url("assets/grocery_crud/js/jquery_plugins/jquery.noty.js"); ?>"></script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ABZ | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css") ?>">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/AdminLTE.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/skins/skin-blue.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/skins/skin-green.min.css"); ?>">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--Notificactions -->

    <!--Edita este css para cambiar colores del tema -->
    <link rel="stylesheet" href="<?php echo base_url("assets/styleTheme.css"); ?>">

</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini color_cornblue"><b>ABZ</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <img style="height: 45px;" src="<?php echo base_url(); ?>assets/iconos/logo abz.png">
            </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?php echo base_url("assets/adminLTE/img/avatar.png"); ?>" class="user-image"
                                 alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                                <?php echo $data['admin_name']; ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?php echo base_url("assets/adminLTE/img/avatar.png"); ?>" class="img-circle"
                                     alt="User Image">
                                <p>
                                    <?php echo $data['admin_name']; ?>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer" style="text-align:center;">
                                <div>
                                    <a href="<?php echo base_url() . 'logout' ?>" class="btn btn-default btn-flat">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">

                <li class="header">MENÚ</li>


                <li <?php echo (strpos(uri_string(), 'users') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/usuarios' ?>">
                        <i class="fa fa-user-circle-o" style="color:#794a9d;"></i> <span>Usuarios</span></a>
                </li>

                <li <?php echo (strpos(uri_string(), 'agencias') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/agencias' ?>">
                        <i class="fa fa-users" style="color:#15649d;"></i> <span>Agencias</span></a>
                </li>


                <li <?php echo (strpos(uri_string(), 'imagenes_background') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/imagenes_background' ?>">
                        <i class="fa fa-picture-o" style="color:#339d89;"></i> <span>Imagenes background</span></a>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bookmark" style="color:#9d2e29;"></i>
                        <span>Editorial</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li <?php echo (strpos(uri_string(), 'banner') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/banner' ?>">
                               <span>Banner</span>
                            </a>
                        </li>
                        <li <?php echo (strpos(uri_string(), 'Editorial') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/Editorial' ?>">
                               <span>Noticias</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li <?php echo (strpos(uri_string(), 'eventos') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/eventos' ?>">
                        <i class="fa fa-calendar-o" style="color:#279d3a;"></i> <span>Eventos</span></a>
                </li>

                <li <?php echo (strpos(uri_string(), 'Travel_Stock') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/Travel_Stock' ?>">
                        <i class="fa fa-ticket" style="color:#cbc735;"></i> <span>Travel Stock</span></a>
                </li>

                <li <?php echo (strpos(uri_string(), 'ABZ_Turistico') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/ABZ_Turistico' ?>">
                        <i class="fa fa-ticket" style="color:#942ecb;"></i> <span>ABZ Turistico</span></a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-certificate" style="color:#4cf551;"></i> <span>Marcas y servicios</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li <?php echo (strpos(uri_string(), 'marcas') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/marcas' ?>">
                                <span>Marcas</span>
                            </a>
                        </li>

                        <li <?php echo (strpos(uri_string(), 'servicios') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/servicios' ?>">
                                <span>Servicios</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-address-book " style="color:#f55f5f;"></i> <span>Directorio</span>
                        <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                            </span>
                    </a>

                    <ul class="treeview-menu" style="display: none;">
                        <li <?php echo (strpos(uri_string(), 'clientes') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/clientes' ?>">
                                <span>Clientes</span></a>
                        </li>

                        <li <?php echo (strpos(uri_string(), 'plataforma') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dashboard/plataforma' ?>">
                                <span>Plataforma</span></a>
                        </li>
                    </ul>
                </li>

                <li <?php echo (strpos(uri_string(), 'info_abz') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'dashboard/info_abz' ?>">
                        <i class="fa fa-info" style="color:#cb15c8;"></i> <span>Información de ABZ</span></a>
                </li>

                <li <?php echo (strpos(uri_string(), 'logout') !== false) ? 'class="active"' : ""; ?> >
                    <a href="<?php echo base_url() . 'logout' ?>">
                        <i class="fa fa-sign-out" style="color:#444;"></i>
                        <span>Salir</span>
                    </a>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $data['seccion']; ?>
                <small><?php echo $data['seccion_desc']; ?></small>
            </h1>
            <ol class="breadcrumb">
                <?php echo (isset($data['seccion_bread'])) ? $data['seccion_bread'] : ''; ?>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php echo $output; ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2019 <a href="#">ABZ-Backend</a>.</strong> All rights reserved.
    </footer>

    <div class="control-sidebar-bg"></div>

</div>
</body>

<?php if (isset($_SESSION['div_aviso'])) { ?>
    <script type="text/javascript">

        $(function () {

            noty({
                layout: 'top',
                text: '<?php echo $_SESSION['div_aviso']; ?>',
                type: '<?php echo $_SESSION['tipo_aviso']; ?>',
                dismissQueue: true,
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                timeout: 8000
            });

        });
    </script>

    <?php
    unset($_SESSION['tipo_aviso']);
    unset($_SESSION['div_aviso']);

} ?>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.js") ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js") ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/plugins/fastclick/fastclick.js") ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/adminLTE/js/app.min.js"); ?>"></script>


</html>
