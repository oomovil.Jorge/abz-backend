<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class Promotion_model extends CI_Model
{

    const advertisement = 'anuncio';
    const promotion = 'promotion';


    public function __construct()
    {
        parent::__construct();
    }

    public function getFullPath($images, $id)
    {
        foreach ($images as $image) {


            if ($image->url != "" || $image->url != null) {

                $image->url = base_url("assets/uploads/promotions/" . $image->url);

            } else {
                $image->url = base_url("assets/uploads/promotions/No_Image_Available.jpg");
            }
        }

        return $images;
    }

}

