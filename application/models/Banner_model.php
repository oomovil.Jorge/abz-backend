<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class Banner_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
    }

    public function getFullPath($images,$banner_id)
    {

        foreach ($images as $image){

        if ($image->url != "" || $image->url != null) {

            $image->url = base_url("assets/uploads/banner/" . $image->url);

        } else {

            $this->db->where("publicity_id",$banner_id)->update("publicity_banner_imgs", array("url" => "No_Image_Available.jpg"));
            $image->url = base_url("assets/uploads/banner/No_Image_Available.jpg");

        }
        }

        return $images;
    }



}

