<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class Functions_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    function saveFile($last_inset_id, $table, $column_name, $file, $destination, $primary_key = "id")
    {
        $random = $this->randomString(32);

        $ext = new SplFileInfo($file['name']);

        $file_url = $destination . $last_inset_id . "_" . $random . "." . $ext->getExtension();

        move_uploaded_file($file['tmp_name'] , $file_url);

        $this->db->where($primary_key, $last_inset_id)->update($table, array($column_name => $file_url));

        return $file_url;
    }

    function randomString($lenght = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $lenght; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}



