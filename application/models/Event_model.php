<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class Event_model extends CI_Model
{

    const EventWholesales = 'wholesaler';
    const EventAgency = 'agency';

    const ConfirmAssistance = 'confirmado';
    const RejectAssistance =  'rechazado';
    const CancelAssistance =  'cancelado';
    const WaitAnswer = 'espera respuesta';


    public function __construct()
    {
        parent::__construct();
    }


    public function getFullPath($images, $banner_id)
    {

        foreach ($images as $image) {

            if ($image->url != "" || $image->url != null) {

                $image->url = base_url("assets/uploads/events/" . $image->url);

            } else {

                $this->db->where("event_id", $banner_id)->update("events_imgs", array("url" => "No_Image_Available.jpg"));
                $image->url = base_url("assets/uploads/events/No_Image_Available.jpg");

            }
        }

        return $images;
    }


    public function getFullPathImage($image)
    {

            if ($image != "" || $image != null) {

                $image = base_url("assets/uploads/events/" . $image);

            } else {

                $image = base_url("assets/uploads/events/No_Image_Available.jpg");

            }

        return $image;
    }



    public function findEventById($event_id)
    {

        $this->db->select('*')
            ->from('events')
            ->where('id', $event_id)
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }


    public function findAssistanceByIds($event_id,$user_id)
    {

        $this->db->select('*')
            ->from('assistance_events')
            ->where('event_id', $event_id)
            ->where('user_id',  $user_id)
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }

}

