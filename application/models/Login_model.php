<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function login_admin($user, $pass)
    {
        $this->db->select('id,email, name');
        $this->db->from('users');
        $this->db->where('email', $user);
        $this->db->where('password', $pass);
        $this->db->where('role', 'ADMIN');
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
}
