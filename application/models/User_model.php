<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__ . '/../../vendor/autoload.php';

class User_model extends CI_Model
{

    const CONFIG_EMAIL = array(
        "useragent" => "CodeIgniter",
        "mailpath" => "/usr/sbin/sendmail",
        "protocol" => "smtp",
        "smtp_host" => "localhost",
        "smtp_port" => "2525",
        "smtp_user" => "abz@oomovilapps.com",
        "smtp_pass" => "-PXNXse1B;5N",
        "mailtype" => "html",
        "charset" => "utf-8",
        "newline" => "\r\n",
        "crlf" => "\r\n",
        "wordwrap" => TRUE
    );


    public function __construct()
    {
        parent::__construct();
    }

    public function checkPassword($user, $password)
    {
        if (md5($password) == $user->password) {
            return true;
        }
        return false;
    }

    public function facebook($data)
    {

        $user = $this->findUserByMail($data['email'], 'facebook');

        if ($user) {
            $user_id = $user->id;
        } else {

            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();

            $file_name = $user_id . '_' . str_replace(' ', '_', $this->randomString()) . ".jpeg";

            $imgFB = file_get_contents($data['img']);
            file_put_contents(__DIR__ . "/../../assets/uploads/users/$file_name", $imgFB);

            $this->db->where("id", $user_id)->update("users", array("img" => $file_name));
        }

        return $this->getFullPath($this->findUserById($user_id));
    }

    public function google($data)
    {

        $user = $this->findUserByMail($data['email'], 'google');

        if ($user) {

            $user_id = $user->id;

        } else {

            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();

            $file_name = $user_id . '_' . str_replace(' ', '_', $this->randomString()) . ".jpeg";

            if ($data['img'] !== "") {

                $img = file_get_contents($data['img']);
                file_put_contents(__DIR__ . "/../../assets/uploads/users/$file_name", $img);
                $this->db->where("id", $user_id)->update("users", array("img" => $file_name));
            }
        }

        return $this->getFullPath($this->findUserById($user_id));
    }

    public function getFullPath($user)
    {


        if ($user->img != "" || $user->img != null) {

            $user->img = base_url("assets/uploads/users/" . $user->img);

        } else {

            $user->img = "nophotoavalible.png";
            $this->db->where("id", $user->id)->update("users", array("img" => $user->img));
            $user->img = base_url("assets/uploads/users/nophotoavalible.png");

        }

        return $user;
    }

    public function findUserById($id)
    {

        $this->db->select('*')
            ->from('users')
            ->where('id', $id)
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }

    }

    public function getInvitationsByUser($id)
    {



        $this->db->select('ae.id as invitation_id,u.id as user_id,e.id as event_id,u.name,u.email,
                           e.title as title_event,e.description,s.name as state,c.name as city,e.date_event,e.visibility,e.status as event_status,
                           ae.status as invitation_status')
            ->from('assistance_events ae')
            ->join('events e', 'e.id = ae.event_id')
            ->join('users u', 'u.id = ae.user_id')
            ->join('states s', 'e.state_id = s.id')
            ->join('cities c', 'e.city_id = c.id')
            ->where('ae.user_id', $id);

        $query = $this->db->get();

        $invitations = array();

        if ($query->num_rows() >= 1) {

            $this->load->model('Event_model');

            foreach ($query->result() as $user_invitation) {


                $invitation['invitation_id'] = $user_invitation->invitation_id;
                $invitation['event_id'] = $user_invitation->event_id;
                $invitation['user_id'] = $user_invitation->user_id;
                $invitation['title']    = 'Has sido invitado a un evento';
                $invitation['message']  = "¡Te han invitado al evento $user_invitation->title_event , por favor confirma tu asistencia!";
                $invitation['visibility'] = $user_invitation->visibility;
                $invitation['event_status'] = $user_invitation->event_status;
                $invitation['invitation_status'] = $user_invitation->invitation_status;


                $this->db->select('*')
                    ->from('events_imgs')
                    ->where('event_id', $user_invitation->event_id)
                    ->limit(1);

                $query = $this->db->get();

                if ($query->num_rows() == 1) {

                    $image =$query->row();

                    $invitation['img'] = $this->Event_model->getFullPathImage($image->url);

                } else {
                    $invitation['img'] = base_url("assets/uploads/events/No_Image_Available.jpg");
                }

                $invitations[] = $invitation;

                }

            return $invitations;
        } else {
            return FALSE;
        }

    }

    public function findUserByMail($email, $typeAccount)
    {
        $this->db->select('*')
            ->from('users')
            ->where('email', $email)
            ->where('typeAccount', $typeAccount)
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }

    }

    public function updatePassword($email){

        $newPass = $this->randomString();

        $this->db->where('email', $email)->update('users', array("password" => md5($newPass)));

        if ($this->db->affected_rows() === 1) {

            $result = $newPass;
        } else {
            $result = false;
        }

        return $result;
    }

    public function updateOs($id, $os)
    {
        $this->db->where('id', $id)->update('users', array("os" => $os));

        return $this->db->affected_rows() === 1 ? true : false;
    }

    public function randomString(){
        
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array

        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


}

