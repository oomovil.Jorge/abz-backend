<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once "funciones.php";

class Dashboard extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');

        $this->load->model('Event_model');
    }

    public function output_view($output = null)
    {
        if ($this->session->userdata('user')) {

            $this->load->view('BE/administradores', (array)$output);

        } else {
            redirect(base_url() . 'logout');
            die();
        }
    }


    //Usuarios
    public function usuarios()
    {

        $data = array(
            "seccion" => "Usuarios",
            "seccion_desc" => "Usuarios",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('users');
        $crud->set_subject('Usuario');


        $crud->columns('img', 'email', 'name', 'job', 'phone', 'role', 'city','type_agency','agency_name');
        $crud->fields('img', 'email', 'password', 'name', 'job', 'phone', 'city', 'role','type_agency','agency_name');
        $crud->required_fields('email', 'password', 'name', 'role','type_agency','agency_name');

        $crud->display_as('id', 'id');
        $crud->display_as('email', 'Correo');
        $crud->display_as('name', 'Nombre');
        $crud->display_as('job', 'Trabajo');
        $crud->display_as('phone', 'Teléfono');
        $crud->display_as('role', 'Privilegios');
        $crud->display_as('city', 'Ciudad');
        $crud->display_as('password', 'Contraseña');
        $crud->display_as('typeAccount', 'registrado con');
        $crud->display_as('agency_name', 'Nombre Agencia');
        $crud->display_as('type_agency', 'Tipo Agencia');

        $crud->field_type('password', 'password');

        $crud->set_relation('type_agency', 'kind_agencies','name');

        $crud->set_field_upload('img', 'assets/uploads/users');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $crud->callback_before_insert(array($this, 'default_values_user_callback'));
        $crud->callback_before_update(array($this, 'default_values_user_callback'));


        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    function default_values_user_callback($post_array)
    {
        if ($post_array['img'] == null) {
            $post_array['img'] = "nophotoavalible.png";
        }

        $post_array['os'] = "WEB";

        $post_array['password'] = md5($post_array['password']);
        return $post_array;
    }

    public function banner(){
        $data = array(
            "seccion" => "Banner",
            "seccion_desc" => "Banner",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('banner');

        $crud->set_subject('Banner');

        $crud->fields( 'img');
        $crud->columns('img');
        $crud->required_fields( 'img');

        $crud->display_as('img', 'Imagen');

        $crud->unset_texteditor('description');

        $crud->set_field_upload('img', 'assets/uploads/banner');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    //Editorial
    public function Editorial()
    {
        $data = array(
            "seccion" => "Editorial",
            "seccion_desc" => "Editorial",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('editorial');

        $crud->set_subject('Editorial');

        $crud->fields('title', 'description', 'img', 'url_facebook', 'url_video', 'url_postcast', 'category', 'principal');
        $crud->columns('title', 'description', 'img', 'category', 'principal');
        $crud->required_fields('title', 'description', 'img', 'category', 'principal');

        $crud->display_as('title', 'Titulo');
        $crud->display_as('description', 'Descripción');
        $crud->display_as('img', 'Imagen');
        $crud->display_as('url_facebook', 'Url de facebook');
        $crud->display_as('url_video', 'Url de video');
        $crud->display_as('url_postcast', 'Url de postcast');
        $crud->display_as('category', 'Categoria');
        $crud->display_as('principal', 'Hacer noticia principal');

        $crud->unset_texteditor('description');

        $crud->change_field_type('has_categories', 'true_false');

        $crud->set_field_upload('img', 'assets/uploads/editorial');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $crud->field_type('principal', 'dropdown', array(1 => 'Si', 0=> 'No'));

        $crud->callback_before_insert(array($this, 'one_principal_callback'));
        $crud->callback_before_update(array($this, 'one_principal_callback'));

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);

    }

    function one_principal_callback($post_array){


        $this->db->where('principal', 1)->update("editorial", array("principal" => 0));


        return $post_array;
    }

    public function linkImgEditorial($primary_key, $row)
    {
        return base_url() . 'dashboard/imagenes_Editorial/' . $row->id;
    }

    public function imagenes_Editorial($row)
    {

        if ($this->session->userdata('user')) {

            $data = array(
                "seccion" => '
                <a href="' . base_url() . 'dashboard/Editorial/" >
                   <button class="btn btn-default cancel-button" type="button">
                    <i class="fa fa-arrow-left"></i>Volver a la lista </button>
                      </a><br><br> Imagenes de publicidad ',
                "seccion_desc" => 'Administración de imagenes',
                "admin_name" => $this->session->user['name'],
                "admin_email" => $this->session->user['email']
            );


            $crud = new grocery_CRUD();

            $crud->set_language("spanish");

            $crud->set_table('publicity_banner_imgs')->where('publicity_id', $row);
            $crud->set_subject('imagenes de banner publicitario');

            $crud->fields('url', 'publicity_id');
            $crud->columns('url');
            $crud->required_fields('url');

            $crud->set_field_upload('url', 'assets/uploads/banner');
            $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

            $crud->field_type('publicity_id', 'hidden', $row);

            $output = $crud->render();
            $output->data = $data;
            $this->output_view($output);

        }
    }

    //agencies
    public function agencias()
    {
        $data = array(
            "seccion" => "Catálogo de agencias",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('agencies');
        $crud->set_subject('');

        $crud->fields('img', 'name', 'contact_name', 'phone', 'email', 'type');
        $crud->columns('img', 'name', 'contact_name', 'phone', 'email', 'type');
        $crud->required_fields('img', 'name', 'contact_name', 'phone', 'email', 'type');


        $crud->display_as('name', 'Nombre de agencia');
        $crud->display_as('contact_name', 'Nombre de contacto');
        $crud->display_as('phone', 'Teléfono de contacto');
        $crud->display_as('email', 'Correo de contacto');
        $crud->display_as('img', 'imagen de agencia');
        $crud->display_as('type', 'tipo de agencia');

        $crud->set_field_upload('img', 'assets/uploads/agencies');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $crud->display_as('type', 'Tipo');
        $crud->field_type('type', 'dropdown', array('Agencia' => 'Agencia', 'Mayorista' => 'Mayorista'));


        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    public function eventos()
    {
        $data = array(
            "seccion" => "Catálogo de eventos de agencias",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('events');
        $crud->set_subject('');

        $crud->fields('agency_id', 'title', 'description', 'direction', 'state_id', 'city_id', 'date_event', 'visibility', 'video_url','podcasts', 'url_social', 'status');
        $crud->columns('agency_id', 'title', 'direction', 'state_id', 'city_id', 'date_event', 'visibility', 'status');
        $crud->required_fields('agency_id', 'title', 'description', 'direction', 'state_id', 'city_id', 'date_event', 'visibility', 'status');

        $crud->display_as('title', 'Nombre del evento');
        $crud->display_as('description', 'Descripción del evento');

        $crud->display_as('visibility', 'Tipo');
        $crud->field_type('visibility', 'dropdown', array('público' => 'público', 'privado' => 'privado'));

        $crud->display_as('direction', 'Dirección');
        $crud->display_as('date_event', 'Fecha de evento');
        $crud->display_as('status', 'Estatus');
        $crud->display_as('state_id', 'Estado');
        $crud->display_as('city_id', 'Ciudad');
        $crud->display_as('url_social', 'Url Facebook');
        $crud->display_as('video_url', 'Url del video');
        $crud->display_as('agency_id', 'Agencia');

        $crud->unset_texteditor('description');

        $crud->add_action('Imagenes', '', '', 'fa-picture-o', array($this, 'linkImgEventos'));
        $crud->add_action('Invitar', '', '', 'fa fa-users', array($this, 'InvitarEvento'));
        $crud->add_action('Confirmados', '', '', 'fa fa-check', array($this, 'ConfirmadosEvento'));

        $crud->set_relation('agency_id', 'agencies', 'name');
        $crud->set_relation('state_id', 'states', 'name');
        $crud->set_relation('city_id', 'cities', 'name');


        $this->load->library('gc_dependent_select');

        $fields = array(
            'state_id' => array(
                'table_name' => 'states',
                'title' => 'name',
                'relate' => null
            ),
            'city_id' => array(
                'table_name' => 'cities',
                'title' => 'name',
                'id_field' => 'id',
                'relate' => 'state_id',
                'data-placeholder' => 'Seleccióna la ciudad'
            )
        );

        $config = array(
            'main_table' => 'cities',
            'main_table_primary' => 'state_id',
            "url" => base_url() . __CLASS__ . '/' . __FUNCTION__ . '/',
        );
        $categories = new gc_dependent_select($crud, $fields, $config);


        $output = $crud->render();
        $output->data = $data;
        $js = $categories->get_js();
        $output->output .= $js;

        $this->output_view($output);
    }

    public function linkImgEventos($primary_key, $row)
    {
        return base_url() . 'dashboard/imagenes_eventos/' . $row->id;
    }

    public function InvitarEvento($primary_key, $row)
    {
        return base_url() . 'dashboard/invitar_evento/' . $primary_key;
    }

    public function ConfirmadosEvento($primary_key, $row)
    {
        return base_url() . 'dashboard/confirmados_evento/' . $primary_key;
    }

    public function imagenes_eventos($row)
    {

        if ($this->session->userdata('user')) {

            $data = array(
                "seccion" => '
                <a href="' . base_url() . 'dashboard/eventos/" >
                   <button class="btn btn-default cancel-button" type="button">
                    <i class="fa fa-arrow-left"></i>Volver a la lista </button>
                      </a>
                      <br>
                      <br> Imagenes del evento ',
                "seccion_desc" => 'Administración de imagenes',
                "admin_name" => $this->session->user['name'],
                "admin_email" => $this->session->user['email']
            );


            $crud = new grocery_CRUD();

            $crud->set_language("spanish");

            $crud->set_table('events_imgs')->where('event_id', $row);
            $crud->set_subject('imagenes de eventos');

            $crud->fields('url', 'event_id');
            $crud->columns('url');
            $crud->required_fields('url');

            $crud->set_field_upload('url', 'assets/uploads/events');

            $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

            $crud->field_type('event_id', 'hidden', $row);

            $output = $crud->render();
            $output->data = $data;
            $this->output_view($output);

        }
    }

    public function invitar_evento($row)
    {

        if ($this->session->userdata('user')) {

            $query = $this->db->select('title')->from('events')->where('id', $row)->limit(1)->get();

            $data = array(
                "seccion" => '
                <a href="' . base_url() . 'dashboard/eventos/" >
                   <button class="btn btn-default cancel-button" type="button">
                    <i class="fa fa-arrow-left"></i>Volver a la lista </button>
                      </a>
                      <br>
                      <br> Invitaciones al evento ' . $query->row()->title,
                "seccion_desc" => 'Administración de invitaciones',
                "admin_name" => $this->session->user['name'],
                "admin_email" => $this->session->user['email']
            );


            $crud = new grocery_CRUD();

            $crud->set_language("spanish");
            $crud->set_table('users');
            $crud->set_subject('Lista de usuarios');

            $crud->columns('img', 'email', 'name', 'job', 'phone', 'city', 'typeAccount');
            $crud->fields('id', 'img', 'email', 'name', 'job', 'phone', 'city', 'role');

            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_edit();

            $crud->set_field_upload('img', 'assets/uploads/users');

            $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

            $crud->add_action('Invitar al evento', '', '', 'fa fa-bell', array($this, 'enviarInvitacionUrl'));


            $output = $crud->render();
            $output->data = $data;
            $this->output_view($output);

        }
    }

    public function enviarInvitacionUrl($primary_key)
    {

        $key = $this->uri->segment_array();

        $event_id = $key[3];

        return base_url() . "dashboard/enviarInvitacion/$event_id/$primary_key";

    }

    public function enviarInvitacion($event_id, $user_id)
    {

        $this->db->select('*')
            ->from('assistance_events')
            ->where(array('event_id' => $event_id, 'user_id' => $user_id))
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 0) {

            $this->db->insert('assistance_events', array(
                'event_id' => $event_id,
                'user_id' => $user_id,
                'status' => Event_model::WaitAnswer,
            ));

            $invitation_id = $this->db->insert_id();
        } else {
            $invitation_id = $query->row()->id;
        }

        //Enviar Notificacion de invitacion
        $tokens = array();

        $this->db->select('*')
            ->from('users')
            ->join('user_token', 'user_token.user_id = users.id')
            ->where('users.id', $user_id);

        $userInfoTokens = $this->db->get();

        if ($userInfoTokens->num_rows() > 0) {

            //obtener  tokens
            foreach ($userInfoTokens->result() as $token) {
                $tokens[] = $token->token;
            }

            //obtener datos del evento
            $event = $this->db->select('*')
                ->from('events e')
                ->join('events_imgs ei', 'ei.event_id = e.id', 'left')
                ->where('e.id', $event_id)
                ->limit(1)
                ->get()
                ->row();

            $nameEvent = $event->title;

            $data = array(
                'title' => 'Has sido invitado a un evento',
                'message' => "¡Te han invitado a el evento $nameEvent , por favor confirma tu asistencia!",
                'img_event' => base_url("assets/uploads/events/" . $event->url),
                'event_id' => $event_id,
                'invitation_id' => $invitation_id,
            );

            $notification = $this->send_notification($tokens, $data);
            print_r($notification);
        }

        $_SESSION["tipo_aviso"] = "success";
        $_SESSION["div_aviso"] = '¡Invitación enviada con exito!';


        redirect(base_url() . 'dashboard/invitar_evento/' . $event_id);
    }

    public function confirmados_evento($row)
    {

        if ($this->session->userdata('user')) {

            $query = $this->db->select('title')->from('events')->where('id', $row)->limit(1)->get();

            $data = array(
                "seccion" => '
                <a href="' . base_url() . 'dashboard/eventos/" >
                   <button class="btn btn-default cancel-button" type="button">
                    <i class="fa fa-arrow-left"></i>Volver a la lista </button>
                </a>
                      <br>
                      <br> 
                      Confirmados al evento: ' . $query->row()->title,
                "seccion_desc" => 'Administración de invitaciones',
                "admin_name" => $this->session->user['name'],
                "admin_email" => $this->session->user['email']
            );


            $crud = new grocery_CRUD();

            $crud->set_language("spanish");


            $crud->set_table('assistance_events')->where('event_id', $row);
            $crud->set_subject('Lista de usuarios');


            $crud->columns('user_id', 'status');

            $crud->set_relation('user_id', 'users', 'email');


            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_edit();
            // $crud->unset_view();
            $crud->display_as('user_id', 'Usuario');
            $crud->display_as('status', 'Estatus');

            //  $crud->field_type('event_id', 'hidden', $row);

            $output = $crud->render();
            $output->data = $data;
            $this->output_view($output);

        }
    }


    //Promociones
    public function Travel_Stock()
    {
        $data = array(
            "seccion" => "Travel Stock",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('promotions_ads');
        $crud->set_subject('');

        $crud->fields('title', 'description', 'init_date', 'final_date', 'type', 'telephone', 'page_web', 'fb', 'agency_id');
        $crud->columns('title', 'description', 'init_date', 'final_date', 'type', 'telephone', 'agency_id');
        $crud->required_fields('title', 'description', 'init_date', 'final_date', 'type', 'telephone', 'agency_id');

        $crud->display_as('title', 'Nombre');
        $crud->display_as('description', 'Descripción ');
        $crud->display_as('init_date', 'Fecha inicial');
        $crud->display_as('final_date', 'Fecha final');
        $crud->display_as('type', 'Tipo de promoción');
        $crud->display_as('telephone', 'teléfono');
        $crud->display_as('page_web', 'pagina web');
        $crud->display_as('fb', 'facebook');
        $crud->display_as('agency_id', 'Agencia');

        $crud->set_relation('agency_id', 'agencies', 'name');

        $crud->unset_texteditor('description');

        $crud->add_action('Imagenes', '', '', 'fa-picture-o', array($this, 'linkImgPromociones'));


        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    public function ABZ_Turistico()
    {
        $data = array(
            "seccion" => "ABZ Turistico",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('tourist_abz');
        $crud->set_subject('');

        $crud->fields('title', 'text', 'img', 'url');
        $crud->columns('title', 'text', 'img', 'url');
        $crud->required_fields('title', 'text', 'img', 'url');

        $crud->display_as('title', 'Nombre');
        $crud->display_as('text', 'Contenido ');
        $crud->display_as('img', 'Imagen');
        $crud->display_as('url', 'Url');

        $crud->unset_texteditor('text');

        $crud->set_field_upload('img', 'assets/uploads/tourist_abz');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    public function linkImgPromociones($primary_key, $row)
    {
        return base_url() . 'dashboard/imagenes_promociones/' . $row->id;
    }

    public function imagenes_promociones($row)
    {

        if ($this->session->userdata('user')) {

            $data = array(
                "seccion" => '
                <a href="' . base_url() . 'dashboard/Travel_Strock/" >
                   <button class="btn btn-default cancel-button" type="button">
                    <i class="fa fa-arrow-left"></i>Volver a la lista </button>
                      </a>
                      <br>
                      <br> Imagenes de la promoción ',
                "seccion_desc" => 'Administración de imagenes',
                "admin_name" => $this->session->user['name'],
                "admin_email" => $this->session->user['email']
            );


            $crud = new grocery_CRUD();

            $crud->set_language("spanish");

            $crud->set_table('promotions_ads_img')->where('promotion_id', $row);
            $crud->set_subject('imagenes de promociones');

            $crud->fields('url', 'promotion_id');
            $crud->columns('url');
            $crud->required_fields('url');

            $crud->set_field_upload('url', 'assets/uploads/promotions');

            $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

            $crud->field_type('promotion_id', 'hidden', $row);

            $output = $crud->render();
            $output->data = $data;
            $this->output_view($output);

        }
    }

    //imagenes background
    public function imagenes_background()
    {
        $data = array(
            "seccion" => "Imagenes background",
            "seccion_desc" => "Imagenes background",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('background_images');
        $crud->set_subject('Imagenes background');

        $crud->fields('url');
        $crud->columns('url');
        $crud->required_fields('url');

        $crud->display_as('url', 'Imagenes');

        $crud->set_field_upload('url', 'assets/uploads/backgroundImages');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    //Directorio Cliente
    public function clientes()
    {
        $data = array(
            "seccion" => "Directorio",
            "seccion_desc" => "Clientes",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('clients');
        $crud->set_subject('Directorio - Clientes');


        $crud->fields('name','img','description','url','cellphone','category');
        $crud->columns('name','img','description','url','cellphone','category');
        $crud->required_fields('name','img','description','url','cellphone','category');

        $crud->display_as('name', 'Nombre');
        $crud->display_as('img', 'Imagen');
        $crud->display_as('description', 'Descripción');
        $crud->display_as('url', 'Pagina web');
        $crud->display_as('category', 'Categoria');
        $crud->display_as('cellphone', 'Teléfono');

        $crud->unset_texteditor('description');

        $crud->set_field_upload('img', 'assets/uploads/clients');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    //Directorio plataforma
    public function plataforma()
    {
        $data = array(
            "seccion" => "Directorio",
            "seccion_desc" => "Plataforma",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('plataform');
        $crud->set_subject('Directorio - Plataforma');


        $crud->fields('name','img','description','cellphone','url');
        $crud->columns('name','img','description','cellphone','url');
        $crud->required_fields('name','img','description','cellphone','url');

        $crud->display_as('name', 'Nombre');
        $crud->display_as('img', 'Imagen');
        $crud->display_as('description', 'Descripción');
        $crud->display_as('url', 'Pagina web');
        $crud->display_as('cellphone', 'Teléfono');


        $crud->unset_texteditor('description');

        $crud->set_field_upload('img', 'assets/uploads/plataform');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    //info_abz
    function info_abz(){
        $data = array(
            "seccion" => "Información ABZ",
            "seccion_desc" => "Información ABZ",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('contact_info_abz');
        $crud->set_subject('Información ABZ');


        $crud->fields('web','facebook','instagram','twitter','cellphone','email','map','state');
        $crud->columns('web','facebook','instagram','twitter','cellphone','email','map');
        $crud->required_fields('web','facebook','instagram','twitter','cellphone','email','map');

        $crud->display_as('web', 'Web ABZ');
        $crud->display_as('cellphone', 'Teléfono');
        $crud->display_as('email', 'Correo');
        $crud->display_as('map', 'Mapa');
        $crud->display_as('state', 'Estados');

        $crud->unset_add();
        $crud->unset_delete();

        $crud->set_relation_n_n("state", "states_contact_abz",
            "states", "info_id", "state_id",
            "name", null);

        $crud->set_field_upload('map', 'assets/uploads/contact_abz');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    //marcas y servicios
    function marcas(){

        $data = array(
            "seccion" => "Marcas",
            "seccion_desc" => "Marcas",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('brands');
        $crud->set_subject('Marcas');


        $crud->fields('name','description','img','web','facebook');
        $crud->columns('name','description','img','web','facebook');
        $crud->required_fields('name','description','img','web','facebook');

        $crud->display_as('name', 'Nombre');
        $crud->display_as('description', 'Descripción');
        $crud->display_as('img', 'Imagen');
        $crud->display_as('web', 'Web');
        $crud->display_as('facebook', 'Facebook');

        $crud->unset_texteditor('description');

        $crud->set_field_upload('img', 'assets/uploads/brands');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }

    function servicios(){
        $data = array(
            "seccion" => "Servicios",
            "seccion_desc" => "Servicios",
            "admin_name" => $this->session->user['name'],
            "admin_email" => $this->session->user['email']
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");

        $crud->set_table('service');
        $crud->set_subject('Servicios');


        $crud->fields('url','img');
        $crud->columns('url','img');
        $crud->required_fields('url','img');

        $crud->display_as('url', 'Video');
        $crud->display_as('img', 'Logo');


        $crud->set_field_upload('img', 'assets/uploads/service');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $output = $crud->render();
        $output->data = $data;
        $this->output_view($output);
    }



    private function send_notification($tokens, $data)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $header = array(
            "Content-Type: application/json",
            "Authorization: key=AAAADUXcV1o:APA91bEKrkHCHPqF3s-vF8xBwTIzU0kcaCUOOS7Wv_6AKo_lSPE_wvLkOFGWAx2bgDiMps_LbTPrxDk2eZNw9EXZrYi8DShzUCv9Hb5bGRjFR6pb2ZJeZyA_J2H3lk1NBfVVCucy5VvL"
        );

        $fields = array(
            "registration_ids" => $tokens,
            "priority" => "high",

            "notification" => array(
                "title" => $data['title'],
                "body" => $data['message'],
                "sound" => "default",
                "parameters" => $data,
            ),

        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result[] = curl_exec($ch);

        curl_close($ch);

        return $result;

    }


}
