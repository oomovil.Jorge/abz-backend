<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require_once __DIR__ . '/../../vendor/autoload.php';


class Api extends REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Functions_model');
    }

    //Users
    function login_post()
    {
        if ($this->post("email") && $this->post("password") && $this->post("os")) {


            $this->db->select('*')->from('users')
                ->where('email', $this->post("email"))
                ->where('typeAccount', "email")
                ->limit(1);

            $query = $this->db->get();

            if ($query->num_rows() == 1) {

                $user = $query->row();

                if ($user->typeAccount == "facebook" || $user->typeAccount == "google") {
                    $this->response(array("error" => 'Usuario registrado con redes sociales.'), 400);
                }

                if ($this->User_model->checkPassword($user, $this->post("password"), TRUE)) {

                    $this->User_model->updateOs($user->id, $this->post("os"), TRUE);

                    $user = $this->User_model->getFullPath($this->User_model->findUserById($user->id));

                    $this->response(array("success" => $user), 200);

                } else {
                    $this->response(array("error" => 'Usuario/Contraseña invalida'), 202);
                }
            }
            $this->response(array("error" => 'Usuario no encontrado'), 202);
        }

        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    function facebook_post()
    {
        $fb = new \Facebook\Facebook([
            'app_id' => '252538865622067',
            'app_secret' => 'a1cde259390760aec5a8b77a01fd604b',
            'default_graph_version' => 'v2.10',
        ]);

        try {

            $response = $fb->get('/me?fields=id,name,email,picture.width(320).height(320)', $this->post('access_token'));

        } catch (\Facebook\Exceptions\FacebookResponseException $e) {

            $this->response(array("error" => 'Graph returned an error: ' . $e->getMessage()), 400);

        } catch (\Facebook\Exceptions\FacebookSDKException $e) {

            $this->response(array("error" => 'Facebook SDK returned an error: ' . $e->getMessage()), 400);
        }


        $me = $response->getGraphUser();

        $urlImgFacebook = $me->getPicture()->getUrl();

        $user = $this->User_model->facebook(array(
            'name' => $me->getName(),
            'email' => $me->getEmail(),
            'password' => '',
            'img' => $urlImgFacebook,
            'role' => 'USER',
            'os' => $this->post('os'),
            'typeAccount' => 'facebook'
        ), TRUE);

        $this->response(array("success" => $user), 200);


    }

    function google_post()
    {
        if ($this->post("name") && $this->post("email") && $this->post("os")) {


            $user = $this->User_model->google(array(
                'name' => $this->post("name"),
                'email' => $this->post("email"),
                'password' => '',
                'img' => $this->post("img_url"),
                'role' => 'USER',
                'os' => $this->post("os"),
                'typeAccount' => 'google',
            ), TRUE);


            $this->response(array("success" => $user), 200);
        }
        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    function createUser_post()
    {
        if ($this->post("name") && $this->post("email") && $this->post("password") && $this->post('os')) {


            if ($this->User_model->findUserByMail($this->post("email"), 'email')) {
                $this->response(array("error" => 'Email ya registrado'), 202);
            }


            $data = array(
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'password' => md5($this->post('password')),
                'img' => "nophotoavalible.png",
                'job' => $this->post('job') ? $this->post('job') : null,
                'phone' => $this->post('phone') ? $this->post('phone') : null,
                'role' => 'USER',
                'typeAccount' => 'email',
                'os' => $this->post('os')
            );

            $this->db->insert('users', $data);

            $user_id = $this->db->insert_id();

            if (isset($_FILES['img']) && $_FILES['img'] != null) {


                $random = $this->User_model->randomString();

                $file_img = $this->post('user_id') . '_' . $random . '_' . str_replace(' ', '_', $_FILES['img']['name']);


                move_uploaded_file($_FILES['img']['tmp_name'], 'assets/uploads/users/' . $file_img);

                $this->db->where("id", $user_id)->update("users", array("img" => $file_img));
            }

            $user = $this->User_model->getFullPath($this->User_model->findUserById($user_id, TRUE), TRUE);

            if ($user) {


                $this->response(array("success" => $user), 200);
            }

        }

        $this->response(array("error" => 'Faltan parametros.'), 400);
    }

    function updateUser_post()
    {

        if ($this->post("user_id")) {

            $user = $this->User_model->findUserById($this->post('user_id'), TRUE);

            if ($user) {

                if ($this->post("name")) {
                    $this->db->where("id", $this->post('user_id'))->update("users", array("name" => $this->post('name')));
                }

                if ($this->post("password")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("password" => md5($this->post('password'))));
                }

                if ($this->post("job")) {
                    $this->db->where("id", $this->post('user_id'))->update("users", array("job" => $this->post('job')));
                }

                if ($this->post("phone")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("phone" => $this->post('phone')));
                }

                if ($this->post("about_me")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("about_me" => $this->post('about_me')));
                }


                if ($this->post("city")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("city" => $this->post('city')));
                }

                if ($this->post("type_agency")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("type_agency" => $this->post('type_agency')));
                }

                if ($this->post("agency_name")) {

                    $this->db->where("id", $this->post('user_id'))
                        ->update("users", array("agency_name" => $this->post('agency_name')));
                }


                if (isset($_FILES['img']) && $_FILES['img'] != null) {


                    $random = $this->User_model->randomString();

                    $file_img = $this->post('user_id') . '_' . $random . '_' . str_replace(' ', '_', $_FILES['img']['name']);

                    move_uploaded_file($_FILES['img']['tmp_name'], 'assets/uploads/users/' . $file_img);

                    $this->db->where("id", $this->post('user_id'))->update("users", array("img" => $file_img));
                }


                $user = $this->User_model->getFullPath($this->User_model->findUserById($this->post("user_id")));

                $this->response(array("success" => $user), 200);
            }
            $this->response(array("error" => 'Usuario no encontrado.'), 400);
        }
        $this->response(array("error" => 'Faltan parametros.'), 400);
    }

    function recoverPassword_post()
    {
        if ($this->post("email")) {


            $this->db->select('*')->from('users')->where('email', $this->post("email"))->limit(1);

            $query = $this->db->get();

            if ($query->num_rows() == 1) {

                $user = $query->row();

                if ($user->password == "facebook" || $user->password == "google") {
                    $this->response(array("error" => 'Usuario registrado con redes sociales.'), 400);
                }

                $updated = $this->User_model->updatePassword($user->email);

                if ($updated) {

                    $this->load->library('email');
                    $this->email->initialize(User_model::CONFIG_EMAIL);
                    $this->email->from('no-reply@abz.com', 'ABZ APP');
                    $this->email->to($this->input->post("email", TRUE));
                    $this->email->subject('Solicitud de nuevo password');

                    $data = array(
                        "newPassword" => $updated,
                    );

                    $body = $this->load->view('emails/web_esp_pass', $data, TRUE);
                    $this->email->message($body);
                    $enviado = $this->email->send();

                    if ($enviado) {
                        $this->response(array("success" => "Mail enviado correctamente."), 200);
                    } else {
                        $this->response(array("error" => "Error al enviar el correo."), 400);
                    }
                }
            } else {
                $this->response(array("error" => "Usuario no encontrado."), 400);
            }

        } else {
            $this->response(array("error" => "No se esta enviando los parametros necesarios."), 400);
        }
    }

    function GetAvailableCategoriesForUsers_get()
    {

        $query = $this->db->select('*')->from('kind_agencies')->order_by("name", "asc")->get();

        if ($query->num_rows() >= 1) {

            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Sin Categorias'), 400);
    }

    //Save user token firebase
    function addtoken_post()
    {
        if ($this->post("user_id") && $this->post("token")) {

            if (!$this->User_model->findUserById($this->post("user_id"))) {
                $this->response(array("error" => 'Usuario no encontrado'), 400);
            }

            $this->db->select('*')->from('user_token')
                ->where('token', $this->post("token"))
                ->limit(1);

            $query = $this->db->get();
            if ($query->num_rows() == 1) {

                $this->db->where("token", $this->post('token'))->update("user_token", array("user_id" => $this->post('user_id')));

                $this->response(array("success" => 'token refrescado'), 202);
            }

            $data = array(
                'token' => $this->post('token'),
                'user_id' => $this->post('user_id'),
            );

            $this->db->insert('user_token', $data);

            $token = $this->db->insert_id();


            if ($token) {
                $this->response(array("success" => 'token guardado'), 200);
            }

        }

        $this->response(array("error" => 'Faltan parametros.'), 400);
    }

    //Banner publicitario
    function publicity_banner_get()
    {
        $query = $this->db->select('*')->from('publicity_banner')->get();

        if ($query->num_rows() >= 1) {

            $this->load->model('Banner_model');

            foreach ($query->result() as $row) {

                $images = $this->db->select('url')->from('publicity_banner_imgs')
                    ->where('publicity_id', $row->id)->get()->result();

                $row->images = $this->Banner_model->getFullPath($images, $row->id);
            }

            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Sin noticias'), 400);
    }

    //listar agencias
    function agencies_get()
    {

        $query = $this->db->select('*')->from('agencies')->order_by("name", "asc")->get();

        if ($query->num_rows() >= 1) {

            foreach ($query->result() as $row) {
                $row->img = base_url("assets/uploads/agencies/" . $row->img);
            }

            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Sin agencias'), 400);
    }

    function findAgenciesById_post()
    {

        if (!$this->post('agency_id')) {
            $this->response(array("error" => "No se esta enviando los parametros necesarios."), 400);
        }
        $this->db->select('*')->from('agencies')->where('id', $this->post('agency_id'))->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $agency = $query->row();

            $agency->img = base_url("assets/uploads/agencies/" . $agency->img);

            $this->response(array("success" => $agency), 200);
        }
        $this->response(array("error" => 'Sin agencias'), 400);
    }

    // Listar eventos
    function events_get()
    {
        $form_complete = false;

        $this->load->model('Event_model');

        $query = $this->db->select('*')->from('events')->get();

        if ($query->num_rows() >= 1) {

            foreach ($query->result() as $row) {

                $images = $this->db->select('url')->from('events_imgs')->where('event_id', $row->id)->get()->result();
                $row->images = $this->Event_model->getFullPath($images, $row->id);

                $row->agency = $this->db->select('*')->from('agencies')->where('id', $query->row()->agency_id)->limit(1)->get()->row();

                if ($this->get("user_id")) {

                    $favorites = $this->db->select('*')
                        ->from('favorite_events')
                        ->where('user_id', $this->get("user_id"))
                        ->where('event_id', $row->id)
                        ->limit(1)
                        ->get();

                    if ($favorites->num_rows() == 1) {

                        $row->favorite = true;
                    } else {
                        $row->favorite = false;
                    }
                }


                if ($this->get("user_id")) {
                    $user = $this->db->select('*')->from('users_confirm_events')->where('user_id', $this->get("user_id"))->limit(1)->get();
                    $form_complete = $user->num_rows() == 1;
                }


            }


            $this->response(array(
                "success" => $query->result(),
                'form_complete' => $form_complete
            ), 200);
        }
        $this->response(array("error" => 'Sin eventos'), 400);
    }

    function getEventById_post()
    {
        $this->load->model('Event_model');

        $query = $this->db->select('*')->from('events')->where('id', $this->input->post('event_id'))->get();

        if ($query->num_rows() == 1) {


            $query->row()->agency =
                $this->db->select('*')
                    ->from('agencies')
                    ->where('id', $query->row()->agency_id)->limit(1)->get()->row();

            $images = $this->db->select('url')->from('events_imgs')->where('event_id', $query->row()->id)->get()->result();
            $query->row()->images = $this->Event_model->getFullPath($images, $query->row()->id);

            if ($this->post('user_id')) {
                $assistance = $this->db->select('*')
                    ->from('assistance_events')
                    ->where('event_id', $query->row()->id)
                    ->where('user_id', $this->post('user_id'))
                    ->limit(1)
                    ->get();

                if ($assistance->num_rows() == 1) {
                    $query->row()->assistence = $assistance->row()->status;
                } else {
                    $query->row()->assistence = 'Sin confirmar';
                }

            }


            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Evento no encontrado'), 400);
    }

    function FavoriteEvent_post()
    {

        if ($this->post("user_id") && $this->post("event_id")) {

            $query = $this->db->select('*')
                ->from('favorite_events')
                ->where('user_id', $this->post("user_id"))
                ->where('event_id', $this->post("event_id"))
                ->limit(1)
                ->get();

            if ($query->num_rows() == 1) {

                $this->db->delete('favorite_events', array('id' => $query->row()->id));

                $this->response(array("success" => 'Evento quitado de favoritos'), 200);

            } else {

                $this->db->insert('favorite_events', array(
                    'event_id' => $this->post("event_id"),
                    'user_id' => $this->post("user_id"),
                ));
                $this->response(array("success" => 'Evento agregado a favoritos'), 200);

            }
        }

        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    //confirmar asistencia a eventos
    function confirm_assistance_event_post()
    {

        if ($this->post("event_id") && $this->post("user_id")) {

            $this->load->model('Event_model');


            $event = $this->Event_model->findEventById($this->post("event_id"));
            if (!$event) {
                $this->response(array("error" => 'Evento no encontrado'), 400);
            }

            $user = $this->User_model->findUserById($this->post("user_id"));
            if (!$user) {
                $this->response(array("error" => 'Usuario no encontrado'), 400);
            }

            $assistance = $this->Event_model->findAssistanceByIds($this->post("event_id"), $this->post("user_id"));
            if ($assistance) {

                $this->db->where('id', $assistance->id)->update('assistance_events', array("status" => Event_model::ConfirmAssistance));
                $this->response(array("success" => 'Asistencia existente : actualizada a confirmada'), 200);

            } else {

                $this->db->insert('assistance_events', array(
                    'event_id' => $event->id,
                    'user_id' => $user->id,
                    'status' => Event_model::ConfirmAssistance,
                ));
                $this->response(array("success" => 'Asistencia confirmada'), 200);
            }
        }
        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    //cancelar o rechazar asistencia a eventos
    function reject_or_cancel_assistance_event_post()
    {

        if ($this->post("event_id") && $this->post("user_id")) {

            $this->load->model('Event_model');


            $event = $this->Event_model->findEventById($this->post("event_id"));
            if (!$event) {
                $this->response(array("error" => 'Evento no encontrado'), 400);
            }

            $user = $this->User_model->findUserById($this->post("user_id"));
            if (!$user) {
                $this->response(array("error" => 'Usuario no encontrado'), 400);
            }

            $assistance = $this->Event_model->findAssistanceByIds($this->post("event_id"), $this->post("user_id"));
            if ($assistance) {

                $this->db->where('id', $assistance->id)->update('assistance_events', array("status" => Event_model::CancelAssistance));
                $this->response(array("success" => 'Asistencia cancelada'), 200);

            } else {

                $this->db->insert('assistance_events', array(
                    'event_id' => $event->id,
                    'user_id' => $user->id,
                    'status' => Event_model::RejectAssistance,
                ));
                $this->response(array("success" => 'Asistencia rechazada'), 200);
            }
        }
        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    //Listar imagenes de background
    function image_background_get()
    {

        $this->db->select('*')->from('background_images')->order_by('id', "desc")->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $image = $query->row();

            $image->url = base_url("assets/uploads/backgroundImages/" . $image->url);

            $this->response(array("success" => $image), 200);
        }
        $this->response(array("error" => 'No se encontraron imagenes'), 400);
    }

    //travel stock
    function promotions_ads_get()
    {

        $this->load->model('Promotion_model');

        if ($this->get('id')) {

            $query = $this->db->select('*')->from('promotions_ads')->where('id', $this->get('id'))->limit(1)->get();

        } else if ($this->get('type')) {

            $query = $this->db->select('*')->from('promotions_ads')->where('type', $this->get('type') == 1 ? 'promocion' : 'anuncio')->get();

        } else {
            $query = $this->db->select('*')->from('promotions_ads')->get();
        }


        if ($query->num_rows() >= 1) {

            $promotions = $query->result();

            foreach ($promotions as $promotion) {

                $promotion->agency =
                    $this->db->select('*')
                        ->from('agencies')
                        ->where('id', $promotion->agency_id)->limit(1)->get()->row();

                $promotion->agency->img = base_url("assets/uploads/agencies/" . $promotion->agency->img);

                unset($promotion->agency_id);

                $images = $this->db->select('url')->from('promotions_ads_img')->where('promotion_id', $promotion->id)->get();

                if ($images->num_rows() >= 1) {

                    $promotion->images = $this->Promotion_model->getFullPath($images->result(), $promotion->id);
                } else {
                    $promotion->images[0] = array('url' => base_url("assets/uploads/promotions/No_Image_Available.jpg"));
                }

            }

            $this->response(array("success" => $promotions), 200);
        }

        $this->response(array("success" => 'Sin promociones'), 200);
    }

    //Solicitar informacion del evento
    function ask_for_information_post()
    {

        if ($this->post("event_id") && $this->post("name") && $this->post("email") && $this->post("title") && $this->post("message")) {


            $this->load->model('Event_model');
            $event = $this->Event_model->findEventById($this->post("event_id"));

            if (!$event) {
                $this->response(array("error" => 'Evento no encontrado'), 400);
            }

            $agency = $this->db->select('*')->from('agencies')->where('id', $event->agency_id)->limit(1)->get()->row();

            $this->load->library('email');
            $this->email->initialize(User_model::CONFIG_EMAIL);
            $this->email->from('no-reply@abz.com', 'ABZ APP');
            $this->email->to($agency->email);
            $this->email->subject('¡Pregunta sobre evento!');

            $data = array(
                "name" => $this->post("name"),
                "email" => $this->post("email"),
                "title" => $this->post("title"),
                "message" => $this->post("message"),
                "event" => $event,
            );

            $body = $this->load->view('emails/email_ask_for_info', $data, TRUE);
            $this->email->message($body);
            $enviado = $this->email->send();

            if ($enviado) {
                $this->response(array("success" => array('exito' => true)), 200);
            }

            $this->response(array("error" => "Error al enviar el correo."), 400);

        }
        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    function getInvitationsByUser_get()
    {
        if ($this->get("user_id")) {

            $user_invitations = $this->User_model->getInvitationsByUser($this->get("user_id", true));
            if ($user_invitations) {


                $this->response(array("success" => $user_invitations), 200);
            }
            $this->response(array("success" => 'Sin invitaciones'), 200);

        }
        $this->response(array("error" => 'Faltan parametros'), 400);
    }

    //ABZ turistico
    function getTouristAbz_get()
    {
        $query = $this->db->select('*')->from('tourist_abz')->get();

        if ($query->num_rows() >= 1) {

            foreach ($query->result() as $item) {
                $item->img = base_url("assets/uploads/tourist_abz/" . $item->img);
            }


            $this->response(array("success" => $query->result()), 200);
        }

        $this->response(array("error" => 'Sin contenido'), 400);


    }

    function touristAbzById_post()
    {

        if (!$this->post('id')) {
            $this->response(array("error" => 'Faltan parametros.'), 400);
        }

        $query = $this->db->select('*')->from('tourist_abz')->where('id', $this->post('id'))->get();

        if ($query->num_rows() == 1) {

            $query->row()->img = base_url("assets/uploads/tourist_abz/" . $query->row()->img);

            $this->response(array("success" => $query->row()), 200);
        }

        $this->response(array("error" => 'No encontrado'), 400);


    }

    //Editorial
    function mainEditorial_get()
    {

        $editorial = $this->db->select('*')->from('editorial')->where('principal', 1)->limit(1)->get();
        $editorial->row()->img = base_url("assets/uploads/editorial/" . $editorial->row()->img);


        $Imgsbanner = $this->db->select('*')->from('banner')->get();

        foreach ($Imgsbanner->result() as $banner) {

            $banner->img = base_url("assets/uploads/banner/" . $banner->img);

        }


        $this->response(array("success" =>
                array(
                    'editorial' => $editorial->row(),
                    'banner' => $Imgsbanner->result()))
            , 200);
    }

    function getEditorial_get()
    {

        if ($this->get("id")) {

            $editorial = $this->db->select('*')->from('editorial')->where('id', $this->get("id"))->get();
            $editorial->row()->img = base_url("assets/uploads/editorial/" . $editorial->row()->img);
            $this->response(array("success" => $editorial->row()), 200);

        }


        if ($this->get("category")) {

            $category = $this->get("category") == 1 ? 'Reporte' : 'Anunciante';

            $query = $this->db->select('*')->from('editorial')->where('category', $category)->get();

        } else {

            $query = $this->db->select('*')->from('editorial')->get();
        }


        if ($query->num_rows() >= 1) {

            foreach ($query->result() as $item) {
                $item->img = base_url("assets/uploads/editorial/" . $item->img);
            }

            $this->response(array("success" => $query->result()), 200);
        }

        $this->response(array("error" => 'Sin registros'), 400);
    }

    //Directorio
    function directoryClients_get()
    {
        $response = null;

        if ($this->get('id')) {

            $client = $this->db->select('*')->from('clients')->where('id', $this->get('id'))->limit(1)->get();

            if ($client->num_rows() == 1) {

                $client->row()->img = base_url("assets/uploads/clients/" . $client->row()->img);

            }


            $response = $client->row();

        } else {

            // 1    'MAYORISTAS INTERNACIONALES',
            // 2  'HOTELES',
            // 3  'LÍNEAS AÉREAS',
            // 4  'LÍNEAS NAVIERAS',
            // 5  'DESTINOS',
            // 6   'CHARTER',
            // 7    'DMC',
            // 8     'OTRAS'


            if ($this->get('category')) {

                $clients = $this->db->select('*')->from('clients')->where('category', (int)$this->get('category'))->get();
            } else {

                $clients = $this->db->select('*')->from('clients')->get();
            }


            foreach ($clients->result() as $client) {
                $client->img = base_url("assets/uploads/clients/" . $client->img);
            }

            $response = $clients->result();
        }

        $this->response(array("success" => $response), 200);
    }

    function directoryPlataform_get()
    {


        $response = null;

        if ($this->get('id')) {

            $plataform = $this->db->select('*')->from('plataform')->where('id', $this->get('id'))->limit(1)->get();

            if ($plataform->num_rows() == 1) {

                $plataform->row()->img = base_url("assets/uploads/plataform/" . $plataform->row()->img);

            }


            $response = $plataform->row();

        } else {


            $plataform = $this->db->select('*')->from('plataform')->get();


            foreach ($plataform->result() as $item) {

                $item->img = base_url("assets/uploads/plataform/" . $item->img);

            }

            $response = $plataform->result();
        }

        $this->response(array("success" => $response), 200);
    }

    // info abz
    function info_abz_get()
    {
        $info = $this->db->select('*')->from('contact_info_abz')->where('id', 1)->limit(1)->get();

        $info->row()->map = base_url("assets/uploads/contact_abz/" . $info->row()->map);


        $this->db->select('states.name')->from('states')->order_by("name", "asc")->get();

        $info->row()->states = $this->db->select('*')->from('states_contact_abz')
            ->join('states', 'states.id = states_contact_abz.state_id')
            ->where('states_contact_abz.info_id', 1)->get()->result();

        $this->response(array("success" => $info->row()), 200);

    }


    function sendMsgToAbz_post()
    {

        if (!$this->post("name") || !$this->post("email") || !$this->post("subject") || !$this->post("msg")) {
            $this->response(array("error" => 'Faltan parametros'), 400);
        }

        $info = $this->db->select('*')->from('contact_info_abz')->where('id', 1)->limit(1)->get();

        if ($info->num_rows() != 1) {
            $this->response(array("error" => 'Por el momento no se pueden enviar mensajes, inténtelo mas tarde'), 400);
        }

        $this->load->library('email');
        $this->email->initialize(User_model::CONFIG_EMAIL);
        $this->email->from('no-reply@abz.com', 'ABZ APP');
        $this->email->to($info->row()->email);
        $this->email->subject('Contacto - ABZ');

        $data = array(
            "name" => $this->post("name"),
            "email" => $this->post("email"),
            "subject" => $this->post("subject"),
            "message" => $this->post("msg"),
        );

        $body = $this->load->view('emails/email_contact', $data, TRUE);
        $this->email->message($body);
        $enviado = $this->email->send();

        if ($enviado) {
            $this->response(array("success" => "Mail enviado correctamente."), 200);
        } else {
            $this->response(array("error" => "Error al enviar el correo."), 400);
        }


    }

    //marcas y servicios

    function brands_get()
    {

        if ($this->get('id')) {

            $brand = $this->db->select('*')->from('brands')->where('id', $this->get('id'))->limit(1)->get();

            if ($brand->num_rows() == 1) {

                $brand->row()->img = base_url("assets/uploads/brands/" . $brand->row()->img);


                $this->response(array("success" => $brand->row()), 200);
            }


            $this->response(array("error" => 'No encontrado'), 400);
        }


        $brands = $this->db->select('*')->from('brands')->get();

        foreach ($brands->result() as $brand) {

            $brand->img = base_url("assets/uploads/brands/" . $brand->img);

        }


        $this->response(array("success" => $brands->result()), 200);
    }

    function service_get()
    {

        if ($this->get('id')) {

            $service = $this->db->select('*')->from('service')->where('id', $this->get('id'))->limit(1)->get();

            if ($service->num_rows() == 1) {

                $service->row()->img = base_url("assets/uploads/service/" . $service->row()->img);


                $this->response(array("success" => $service->row()), 200);
            }


            $this->response(array("error" => 'No encontrado'), 400);
        }


        $services = $this->db->select('*')->from('service')->get();

        foreach ($services->result() as $service) {

            $service->img = base_url("assets/uploads/service/" . $service->img);

        }


        $this->response(array("success" => $services->result()), 200);
    }

    //Ciudades y estados

    function get_states_get()
    {
        /*

         select
          s.country,
          s.name as state,
          s.abbrev as state_abbrev,
          c.name as city
         from states s,city c where s.id = c.state_id

         */

        $query = $this->db->select('*')->from('states')->order_by("name", "asc")->get();

        if ($query->num_rows() >= 1) {

            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Sin agencias'), 400);


    }

    function get_cities_get()
    {


        $query = $this->db->select("*")->from('cities')->order_by("name", "asc")->get();

        if ($query->num_rows() >= 1) {

            $this->response(array("success" => $query->result()), 200);
        }
        $this->response(array("error" => 'Sin agencias'), 400);

    }

    function get_state_from_city_post()
    {
        if (!$this->post("city_id")) {
            $this->response(array("error" => 'Faltan parametros'), 400);
        }


        $query = $this->db->select("
                          s.country,
                          s.name as state,
                          s.abbrev as state_abbrev,
                          c.name as city,
                          s.id as state_id,
                          c.id as city_id")
            ->from('states s,cities c')
            ->where('s.id = c.state_id')
            ->where('c.id', $this->post("city_id"))
            ->get();


        if ($query->num_rows() > 0) {

            $this->response(array("success" => $query->result()), 200);
        }


        $this->response(array("error" => 'Sin resultados'), 400);
    }

    function get_cities_from_state_post()
    {
        if (!$this->post("state_id")) {
            $this->response(array("error" => 'Faltan parametros'), 400);
        }

        $query = $this->db->select("*")->from('cities')->where('state_id', $this->post("state_id"))->get();

        if ($query->num_rows() > 0) {

            $this->response(array("success" => $query->result()), 200);
        }


        $this->response(array("error" => 'Sin resultados'), 400);
    }

    //Form confirm event
    function form_confirm_event_post()
    {
        if (!$this->post("user_id") && !$this->post("user_role")) {
            $this->response(array("error" => 'Faltan parametros'), 400);
        }


        $user = $this->db->select('*')->from('users_confirm_events')->where('user_id', $this->post("user_id"))->limit(1)->get();
        if ($user->num_rows() == 1) {
            $this->response(array("success" => 'Formulario completo'), 202);
        }


        if ($this->post("user_role") == 1) {

            if (!$this->post("name") && !$this->post("address") && !$this->post("city") && !$this->post("web_email")) {
                $this->response(array("error" => 'Faltan parametros'), 400);
            }


            $data = array(
                'user_id' => $this->post('user_id'),
                'user_role' => $this->post('user_role'),
                'name' => $this->post('name'),
                'address' => $this->post('address'),
                'city' => $this->post('city'),
                'web_email' => $this->post('web_email'),
            );

            $this->db->insert('users_confirm_events', $data);

        } elseif ($this->post("user_role") == 2) {

            if (!$this->post("name") && !$this->post("lastname") && !$this->post("job") && !$this->post("web_email")) {
                $this->response(array("error" => 'Faltan parametros'), 400);
            }

            $data = array(
                'user_id' => $this->post('user_id'),
                'user_role' => $this->post('user_role'),
                'name' => $this->post('name'),
                'lastname' => $this->post('lastname'),
                'job' => $this->post('job'),
                'web_email' => $this->post('web_email'),
            );

            $this->db->insert('users_confirm_events', $data);

            $id = $this->db->insert_id();

            if (isset($_FILES['img']) && $_FILES['img'] != null) {
                $this->Functions_model->saveFile($id, 'users_confirm_events', "img", $_FILES['img'], "assets/uploads/form/");
            }

        }


        $user = $this->db->select('*')->from('users_confirm_events')->where('user_id', $this->post("user_id"))->limit(1)->get();
        if ($user->num_rows() == 1) {
            $this->response(array("success" => $user->row()), 200);
        }


        $this->response(array("error" => 'Rol no valido'), 400);
    }


}